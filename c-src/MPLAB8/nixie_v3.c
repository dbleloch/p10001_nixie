/******************************************************************************
 *
 *                Nixie Clock Project
 *
 ******************************************************************************
 * FileName:        nixie_v1.c 
 * Dependencies:    None
 * Processor:       PIC18F2620
 * Compiler:        C18
*******************************************************************************
 * Description
 * i2c bus expander for controlling the nixie elements
 * i2c real time clock
 * future stuff must include i/o to set the clock functions et
 c.
******************************************************************************/

#include <p18f2620.h>
#include <nixie2620.h>
#include <delays.h>
#include <i2c.h>

// #include <sw_uart.h>
// #include <xlcd.h>
// #include <float.h>
// #include <string.h>

/******************************************************************
 *
 *   Sys clock, baud rate and timeout calculations
 *
 * Incoming clock frequency 10MHz; Oscillator period = 100ns
 * x4 PLL enabled to generate 40MHz internal clock
 * Instruction Cycle (4 clk/cycle) TCy 125ns
 *
 * Delay10TCy = 1.25us
 * Delay100TCy = 12.5us
 * Delay1kTCy = 125us
 * Delay10kTCy = 1.25ms
 * Delay10kTCy(40) = 50ms
 * Delay10kTCy(200) = 250ms
 *
 ******************************************************************/

// #pragma config OSC = HSPLL
// #pragma config WDT = OFF
// #pragma config LVP = OFF
// #pragma config DEBUG = ON

 
const rom unsigned char minsx10[10] = {
                              0x5F,  // 0 // U6
                              0x9F,  // 1
                              0x7B,  // 2
                              0xB7,  // 3
                              0xBB,  // 4
                              0xBD,  // 5
                              0x7D,  // 6
                              0xAF,  // 7
                              0x77,  // 8
                              0x6F   // 9

                              };                             
            

const rom unsigned char minsx1[10] = {
                             0xBE,  // 0 // U7
                             0xBD,  // 1
                             0xF6,  // 2
                             0xED,  // 3
                             0xF5,  // 4
                             0xF9,  // 5
                             0xFA,  // 6
                             0xDD,  // 7
                             0xEE,  // 8
                             0xDE   // 9
                             };
                             
                             
const rom unsigned char hrsx10[10] = {
                              0xBD,  // 0 // U6
                              0x7D,  // 1
                              0xAF,  // 2
                              0x77,  // 3
                              0x6F,  // 4
                              0x5F,  // 5
                              0x9F,  // 6
                              0x7B,  // 7
                              0xB7,  // 8
                              0xBB   // 9
                             };

const rom unsigned char hrsx1[10] = {
                             0xBD,  // 0 // U7
                             0xBE,  // 1
                             0xF5,  // 2
                             0xEE,  // 3
                             0xF6,  // 4
                             0xFA,  // 5
                             0xF9,  // 6
                             0xDE,  // 7
                             0xED,  // 8
                             0xDD   // 9
                             };



/******************************************************************************/


/****************************************************************
 ***********************               **************************
 **********************    Functions    *************************
 ***********************               **************************
 ****************************************************************/
 
 
/* setup PIC hardware */
unsigned char SetupPICHardware (void)
    {
    ADCON1 = 0b00001111;   // ADCs off
    ADCON0 = 0b00000000;   // ADCs off

    TRISA = 0b11000000; // switch is an input
    TRISB = 0b00000000;
    TRISC = 0b10100000;
//    LEDs_TRIS = 0;   // LEDs are outputs
        
    //  Count the time between bytes
    /*  bit 7 : TMR0ON = 1 : Timer on            *
     *  bit 6 : T08BIT = 0 : 16 bit              *
     *  bit 5 : T0CS = 0 :  Internal clk         *
     *  bit 4 : T0SE = 0 :  Count on rising edge *
     *  bit 3 : PSA = 0  :  Prescaler used       *
     *  bit 2 : T0PS2 = 0 :                      *
     *  bit 1 : T0PS1 = 1 : 8x prescaler         *
     *  bit 0 : T0PS0 = 0 :                      */
    T0CON = 0b00000000;
    
    // Open I2C Peripheral
    OpenI2C(MASTER, SLEW_OFF);
    
    // Set 100kHz Baud Rate for a 40MHz clock
    SSPCON1 = 0b00111000;
    SSPCON2 = 0b00011000;
    SSPADD =  24; // Baud rate = fosc / (4* SSPADD+1)
    
    return 0;
}

unsigned char SetupPCA9555(void)
    {
        
    /* Configure minutes driver */
    PCA9555_Write(minsAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    /* Configure hours driver */
    PCA9555_Write(hoursAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    return 0;
}

unsigned char SetupPCA8593(void)
    {
    
    /* Reset RTC */
    PORTCbits.RC2 = 0;
    Delay10KTCYx(80);
   
    /* Bring RTC out of reset */
    PORTCbits.RC2 = 1;
    Delay10KTCYx(40);
    
    /* Configure RTC & hold */
    PCF8593_Write(clockAddr, 0x00, 0x80);
    Delay10KTCYx(10);
    
    /* Load time into RTC & start */
    PCF8593_Write(clockAddr, 0x04, 0x00); // hours
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x03, 0x00); // mins
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x02, 0x00); // secs
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x00, 0x00); // start counting
    Delay10KTCYx(10);
    
    return 0;
}

unsigned char BlankDisplay(unsigned char poopy)
    {
    Nop();
}

/* Write to the port expander */
unsigned char PCA9555_Write (unsigned char addr, unsigned char command, unsigned char byte0, unsigned char byte1)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    WriteI2C(byte1);
    StopI2C();
    return 0;
}

/* Write to the rtc */
unsigned char PCF8593_Write (unsigned char addr, unsigned char command, unsigned char byte0)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    StopI2C();
    return 0;
}

unsigned char PCF8593_Read (unsigned char addr, struct tRTCData * RTCR_RTCData, unsigned char debug)
    {
    
    unsigned char minutesRegister;

    // For test mode, display minutes and seconds instead of hours and minutes
    if (debug == 1)
        {
        minutesRegister = 0x02;
    }
    else
        {
        minutesRegister = 0x03;
    }  

    // Get data from the RTC
    StartI2C();
    WriteI2C(addr);
    WriteI2C(minutesRegister);
    StartI2C();
    WriteI2C(addr|1);
    RTCR_RTCData->minutes = ReadI2C();
    AckI2C();
    RTCR_RTCData->hours = ReadI2C();
    NotAckI2C();
    StopI2C();
        
    return 0;
}

unsigned char ReadTime (struct tRTCData * RT_RTCData)
    {
    
    PCF8593_Read (clockAddr, RT_RTCData, 1);
    
    return 0;
}

unsigned char SetTime (struct tRTCData * ST_RTCData)
    {
    
    PCF8593_Read (clockAddr, ST_RTCData, 1);
    
    return 0;
}

unsigned char ScrollTheDigits (void)
    {

    unsigned char cCount;
    struct tRTCData testClock;
    
    for (cCount=0; cCount<99; cCount++)
        {
        testClock.minutes = cCount;
        testClock.hours = cCount;
        
        UpdateDigits(&testClock);
        Delay10KTCYx(100);
    }

    return 0;
}

unsigned char CompareOldNew (struct tRTCData * CON_oldRTCData, struct tRTCData * CON_newRTCData)
    {

    unsigned char rc;

    // Determine whether the new time is different from the old time by looking at the minutes
    if (CON_newRTCData->minutes != CON_oldRTCData->minutes)
        {
        rc = 0;
    }
    else
        rc = 1;
        
    return rc;
}

unsigned char UpdateTime(struct tRTCData * UT_oldRTCData, struct tRTCData * UT_newRTCData)
    {
    UT_oldRTCData->minutes = UT_newRTCData->minutes;
    UT_oldRTCData->hours = UT_newRTCData->hours;
}

unsigned char UpdateDigits(struct tRTCData * UD_RTCData)
    {
    // Local variables
    unsigned char tens_of_hours;
    unsigned char units_of_hours;
    unsigned char tens_of_mins;
    unsigned char units_of_mins;
    
    // Convert BCD hours and minutes into digit values
    //** -- might have to change the mod and div as these are very processor intensive operators -- **//
    //tens_of_hours = UD_RTCData->hours/10;
    //units_of_hours = UD_RTCData->hours%10;
    //tens_of_mins = UD_RTCData->minutes/10;
    //units_of_mins = UD_RTCData->minutes%10;
    
    tens_of_hours = UD_RTCData->hours&0xF0;
    tens_of_hours = tens_of_hours >> 4;
    tens_of_hours = tens_of_hours&0x0F;
    
    units_of_hours = UD_RTCData->hours&0x0F;
    
    tens_of_mins = UD_RTCData->minutes&0xF0;
    tens_of_mins = tens_of_mins >> 4;
    tens_of_mins = tens_of_mins&0x0F;
    
    units_of_mins = UD_RTCData->minutes&0x0F;
    
    // Update the digits with their new values
    PCA9555_Write(minsAddr, 0x02, minsx10[tens_of_mins], minsx1[units_of_mins]);
    PCA9555_Write(hoursAddr, 0x02, hrsx10[tens_of_hours], hrsx1[units_of_hours]);
    
    return 0;
}

// unsigned int read_timeout_timer (void)
//     {
//     
//     unsigned int iTimerLow;
//     unsigned int iTimerHigh;
// 
//     iTimerLow = TMR0L;
//     iTimerHigh = TMR0H;
// 
//     iTimerLow &= 0x00FF;  // Clear all the high bits in the low byte timer
// 
//     iTimerHigh *= 256;      // Shift high byte to top 8 bits
// 
//     iTimerHigh |= iTimerLow;  // Or in the low bits for the full timer reading
// 
//     return iTimerHigh;   
// 
// }


// void clear_timeout_timer (void)
//     {
//     
//     TMR0H = 0;
//     TMR0L = 0;
// 
// }


// void setup_iBus_UART_hardware (void)
//     {
// 
//     /* UART transmit pin */
//     iB_TX_TRIS = 0;
// 
//     /* UART receive pin */
//     iB_RX_TRIS = 1;
// 
//     /* UART sniffer pin */
//     iB_Sniffer_TRIS = 1;
// 
//     /* Set output line high (this means no FET on, iBus line high) ... circuit must be positive logic... low on TX pin = iBus line pulled low */
//     iB_TX_LAT = 1; 
// 
//     //    SPBRG = 25; // Baud rate set for 9600 @ 4MHz
//     //    SPBRG = 103; // Baud rate set for 9600 @ 16MHz
//     SPBRG = 207; // Baud rate set for 9600 @ 32MHz
// 
//     /*  AUSART Transmit Register                 *
//      *  bit 7 : CSRC = 0 : Don't care            *
//      *  bit 6 : TX9 = 1 : 9bit transmission      *
//      *  bit 5 : TXEN = 1 : Transmit ENable      *
//      *  bit 4 : SYNC = 0 : Asynchronous mode     *
//      *  bit 3 : Unimplemented bit = 0            *
//      *  bit 2 : BRGH = 1 : High speed mode       *
//      *  bit 1 : TRMT = 1 : Transmit shift reg    *
//      *  bit 0 : TX9D = 0 : Parity bit            */
//     TXSTA = 0b01100110;
// 
//     /* AUSART Receive Register                   *
//      *  bit 7 : SPEN = 1 : Serial port enabled   *
//      *  bit 6 : RX9 = 1 : 9bit reception         *
//      *  bit 5 : SREN = 0 : Don't care            *
//      *  bit 4 : CREN = 1 : Enable receiver       *
//      *  bit 3 : ADDEN = 0 : 9th bit for parity   *
//      *  bit 2 : FERR = 0 : No framing error      *
//      *  bit 1 : OERR = 0 : No overrun error      *
//      *  bit 0 : RX9D = 0 : 9th data bit          */
//     RCSTA = 0b11010000;
// 
//     /*  BAUDCON Baud Rate Control Reg            *
//      *  bit 7 : ABDOVF = 0 : Clear rollover      *
//      *  bit 6 : RCIDL = 0 : Read only            *
//      *  bit 5 : Unimplemented bit = 0            *
//      *  bit 4 : SCKP = 0 : Asynchronous mode     *
//      *  bit 3 : BRG16 = 0 : 8 bit BRG mode       *
//      *  bit 2 : Unimplemented bit = 0            *
//      *  bit 1 : WUE = 0 : Rx wake up disabled    *
//      *  bit 0 : ABDEN = 0 : No auto baud rate    */
//     BAUDCON = 0b00000000; 
// 
// }


/* Setup Vmusic UART */
// void setup_Vm_UART_hardware (struct CDchanger * CDPlayer)
//     {
// 
//     /* UART transmit pin */
//     VM_UART_TX_TRIS = 0;
//     
//     /* UART receive pin */
//     VM_UART_RX_TRIS = 1;
//     
//     /* Set output line high (then it gets pulled low when a start bit occurs) */
//     VM_UART_TX_LAT = 1;
//         
//     // OpenUART();
//     
//     CDPlayer->status = CD_IDLE;
//     CDPlayer->current_track = 0;
//     CDPlayer->current_disk = 0;
//     
// }


// unsigned char VM_UART_write(unsigned char cmdID)
//     {
//     unsigned char string_pointer_count;
//     unsigned char stringy[14];
// 
//     // copy string required from program memory to data memory
//     strcpypgm2ram (stringy, vm2CMD[cmdID]);
// 
//     // serialize out the string to the UART
//     for (string_pointer_count=0; stringy[string_pointer_count] != '\0'; ++string_pointer_count)
//         {
//         WriteUART(stringy[string_pointer_count]);
//         Delay10TCYx(300);
//     }
// }



/***********************************************************
 ***********************          **************************
 **********************    Main    *************************
 ***********************          **************************
 ***********************************************************/

void main (void)
    {
    
    struct tRTCData newRTCData = {0, 0}; // the time, as freshly recovered from the RTC
    struct tRTCData oldRTCData = {8, 8}; // the current time, most of the time

    // This is the main working iBus frame
    // struct iBusFrame workingFrame;

    // This is the CD changer emulator structure
    // struct CDchanger CDPlayer1;
    
    // Temporary storage for CRC function call
    // unsigned char cTempCRC;
    
    // Matched iBus Frame ID byte
    // unsigned char cMatchedFrameID;
    
    /* used for XLCD GUI - not used */
    // unsigned int adcResult = 0x0000;
    
    /***************************************************************/
    /*              Functions to set up hardware                   */
    /***************************************************************/
    
    /* setup PIC hardware */
    SetupPICHardware ();
    
    SetupPCA9555 ();

    SetupPCA8593 ();

    // ScrollTheDigits ();
    
    /* Set all to 00:00 */
    UpdateDigits(&oldRTCData);
    
    while (1)
        {
        /* Strobe B0 so that we can measure update period */
        PORTBbits.RB0 = '1';
       
        /* Read from RTC */
        ReadTime(&newRTCData);

        /* Update new time */
        if (CompareOldNew(&oldRTCData, &newRTCData) == 0)
            {
            UpdateTime(&oldRTCData, &newRTCData);
            Nop();
            UpdateDigits(&oldRTCData);
        }
        
        /* Unstrobe B0 so that we can measure update time */
        PORTBbits.RB0 = '0';
        
        /* Wait for 50ms */
        //** -- sooner or later, I'm going to have to set up a timer for this and put it in an interrupt -- **//
        Delay10KTCYx(40);
        
    };

    
    /* UART pin definitions and directions */
 //   setup_iBus_UART_hardware (); 
 //   setup_Vm_UART_hardware (&CDPlayer1);
    
    // Open & setup LCD
    // setup_LCD_hardware();
    
    /* setup mode on Vmusic */
    //init_Vmusic (); 
    
    /* Wait for VM to become ready... this needs to be replaced by read from UART at some point... */
 //   Delay10KTCYxx (255);
 //   Delay10KTCYxx (255);
 //   Delay10KTCYxx (255);
    // Delay10KTCYxx (255);
   
 //   workingFrame.source = 0xAA;

    /* Check for hardware line from VM2 to see if it is ready */
    //while (VM_READY != VM_READY) ;
    
    /* Send a frame to the iBus to say that the 'CD-Changer' is ready */
    // memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_UNPOLLED_ANNOUNCE]), 0x24);

    /* Fill the CRC byte */
    // calcCRC(&workingFrame);

    // display_Text(0);
    
    /* Keep sending it until you don't get a collision */ 
 //   while (write_iBus(&workingFrame) != CODE_VALID)
 //       {
 //       wait_iBus_clear_for_15ms();
 //   }
    
 //   while (1)
 //       {
 //       // Wait for a byte to come in from the iBus
 //       read_iBus(&workingFrame);
 //       
 //       if (workingFrame.status == CODE_VALID) // 0x00 is frame valid
 //           {
 //           // Try to match the received frame to one that we recognize
 //           cMatchedFrameID = matchFrame(&workingFrame);
 //           
 //           if (cMatchedFrameID > 0)
 //               {
 //               switch (cMatchedFrameID)
 //                   {
 //                   case 0 : // no valid match
 //                       break;
 //                   case iB_RAD_POLL_CHG : // we rx'd an 'are you there?'; send a 'yes I am'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Send a frame to the iBus to say that the 'CD-Changer' is ready */
 //                       memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_POLLED_ANNOUNCE]), 0x24);
 //                       /* Fill the CRC byte */
 //                       calcCRC(&workingFrame);
 //                       /* Wait til iBus has been high for 15ms then keep sending it until you don't get a collision */ 
 //                       do { wait_iBus_clear_for_15ms(); } while (write_iBus(&workingFrame) != CODE_VALID);
 //                       break;
 //                    case iB_RAD_CURRENT_CHG_STATE : // we rx'd a 'current status?'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Get the right response frame... */
 //                       if (CDPlayer1.status > 0x01) { memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_QUERY_REPLY_NOT_PLAYING]), 0x24); }
 //                       else { memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_QUERY_REPLY_PLAYING]), 0x24); }
 //                       /* ... then fill it with the correct track and disk ID ... */
 //                       workingFrame.data[6] = CDPlayer1.current_disk;
 //                       workingFrame.data[7] = CDPlayer1.current_track;
 //                       /* Fill the CRC byte */
 //                       calcCRC(&workingFrame);
 //                       /* Send the response */
 //                       do { wait_iBus_clear_for_15ms(); } while (write_iBus(&workingFrame) != CODE_VALID);
 //                       break;
 //                    case iB_RAD_CHR_STOP : // we rx'd a 'stop!'
 //                        /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       break;
 //                   case iB_RAD_CHR_START : // we rx'd a 'start!'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Pause the VMusic2 */
 //                       VM_UART_write (VM2_PAUSE);
 //                       break;
 //                   case iB_RAD_CHR_GOTO_CD : // we rx'd a 'goto DD'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       break;
 //                   case iB_RAD_CHR_NEXT_TRACK : // jump to next track
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Next track on the VMusic2 */
 //                       VM_UART_write (VM2_NEXT);
 //                       break;
 //                   case iB_RAD_CHR_PREV_TRACK : // jump back a track
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Previous track on the VMusic2 */
 //                       VM_UART_write (VM2_PREV);
 //                       break;
 //                   case iB_MFD_VOL_DN :
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);          
 //                       break;
 //                   default :
 //                       break;
 //               }
 //           }   
 //       }
 //   }    
}
