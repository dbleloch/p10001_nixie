/******************************************************************************
 *
 *                Nixie Clock Project
 *
 ******************************************************************************
 * FileName:        nixie_v1.c 
 * Dependencies:    None
 * Processor:       PIC18F2620
 * Compiler:        C18
*******************************************************************************
 * Description
 * i2c bus expander for controlling the nixie elements
 * i2c real time clock
 * future stuff must include i/o to set the clock functions et
 c.
******************************************************************************/

#include <p18f2620.h>
#include <nixie2620.h>
#include <delays.h>
#include <timers.h>
#include <i2c.h>

// #include <sw_uart.h>
// #include <xlcd.h>
// #include <float.h>
// #include <string.h>

#define startHours 0x18
#define startMins 0x33

/******************************************************************
 *
 *   Sys clock, baud rate and timeout calculations
 *
 * Incoming clock frequency 10MHz; Oscillator period = 100ns
 * x4 PLL enabled to generate 40MHz internal clock
 * Instruction Cycle (4 clk/cycle) TCy 100ns
 *
 * Delay10TCy = 1us
 * Delay100TCYx = 10us
 * Delay1kTCy = 100us
 * Delay10kTCy = 1ms
 * Delay10kTCy(10) = 10ms
 *
 ******************************************************************/

// #pragma config OSC = HSPLL
// #pragma config WDT = OFF
// #pragma config LVP = OFF
// #pragma config DEBUG = ON

 
const rom unsigned char minsx10[10] = {
                              0x5F,  // 0 // U6
                              0x9F,  // 1
                              0x7B,  // 2
                              0xB7,  // 3
                              0xBB,  // 4
                              0xBD,  // 5
                              0x7D,  // 6
                              0xAF,  // 7
                              0x77,  // 8
                              0x6F   // 9

                              };                             
            

const rom unsigned char minsx1[10] = {
                             0xBE,  // 0 // U7
                             0xBD,  // 1
                             0xF6,  // 2
                             0xED,  // 3
                             0xF5,  // 4
                             0xF9,  // 5
                             0xFA,  // 6
                             0xDD,  // 7
                             0xEE,  // 8
                             0xDE   // 9
                             };
                             
                             
const rom unsigned char hrsx10[10] = {
                              0xBD,  // 0 // U6
                              0x7D,  // 1
                              0xAF,  // 2
                              0x77,  // 3
                              0x6F,  // 4
                              0x5F,  // 5
                              0x9F,  // 6
                              0x7B,  // 7
                              0xB7,  // 8
                              0xBB   // 9
                             };

const rom unsigned char hrsx1[10] = {
                             0xBD,  // 0 // U7
                             0xBE,  // 1
                             0xF5,  // 2
                             0xEE,  // 3
                             0xF6,  // 4
                             0xFA,  // 5
                             0xF9,  // 6
                             0xDE,  // 7
                             0xED,  // 8
                             0xDD   // 9
                             };



/******************************************************************************/


/****************************************************************
 ***********************               **************************
 **********************    Functions    *************************
 ***********************               **************************
 ****************************************************************/
 
 
/* setup PIC hardware */
unsigned char SetupPICHardware (void)
    {
    ADCON1 = 0b00001111;   // ADCs off
    ADCON0 = 0b00000000;   // ADCs off

    TRISA = 0b11000000; // switch is an input
    TRISB = 0b00000000;
    TRISC = 0b10100000; // UART Tx is an output; Rx is an input
    
    // Timer 0
    //OpenTimer0(TIMER_INT_ON & T0_PS_1_256);
    OpenTimer0(0b10000111);
    
    // Open I2C Peripheral
    OpenI2C(MASTER, SLEW_OFF);
    
    // Set 100kHz Baud Rate for a 40MHz clock
    SSPCON1 = 0b00111000;
    SSPCON2 = 0b00011000;
    SSPADD =  24; // Baud rate = fosc / (4* SSPADD+1)
    
    return 0;
}

unsigned char SetupPCA9555(void)
    {
        
    /* Configure minutes driver */
    PCA9555_Write(minsAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    /* Configure hours driver */
    PCA9555_Write(hoursAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    return 0;
}

unsigned char SetupPCF8593(void)
    {
    
    /* Reset RTC */
    PORTCbits.RC2 = 0;
    Delay10KTCYx(80);
   
    /* Bring RTC out of reset */
    PORTCbits.RC2 = 1;
    Delay10KTCYx(40);
    
    /* Configure RTC & hold */
    PCF8593_Write(clockAddr, 0x00, 0x80);
    Delay10KTCYx(10);
    
    /* Load time into RTC & start */
    PCF8593_Write(clockAddr, 0x04, (0x00 | startHours)); // hours
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x03, startMins); // mins
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x02, 0x00); // secs
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x00, 0x00); // start counting
    Delay10KTCYx(10);
    
    return 0;
}

unsigned char BlankDisplay(unsigned char poopy)
    {
    Nop();
}

/* Write to the port expander */
unsigned char PCA9555_Write (unsigned char addr, unsigned char command, unsigned char byte0, unsigned char byte1)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    WriteI2C(byte1);
    StopI2C();
    return 0;
}

/* Write to the rtc */
unsigned char PCF8593_Write (unsigned char addr, unsigned char command, unsigned char byte0)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    StopI2C();
    return 0;
}

unsigned char PCF8593_Read (unsigned char addr, struct tRTCData * RTCR_RTCData, unsigned char debug)
    {
    
    unsigned char minutesRegister;

    // For test mode, display minutes and seconds instead of hours and minutes
    if (debug == 1)
        {
        minutesRegister = 0x02;
    }
    else
        {
        minutesRegister = 0x03;
    }  

    // Get data from the RTC
    StartI2C();
    WriteI2C(addr);
    WriteI2C(minutesRegister);
    StartI2C();
    WriteI2C(addr|1);
    RTCR_RTCData->minutes = ReadI2C();
    AckI2C();
    RTCR_RTCData->hours = (ReadI2C() & 0x3F);
    NotAckI2C();
    StopI2C();
        
    return 0;
}

unsigned char ReadTime (struct tRTCData * RT_RTCData)
    {
    
    PCF8593_Read (clockAddr, RT_RTCData, 0);
    
    return 0;
}

unsigned char SetTime (struct tRTCData * ST_RTCData)
    {
    
    PCF8593_Read (clockAddr, ST_RTCData, 0);
    
    return 0;
}

unsigned char ScrollTheDigits (void)
    {

    unsigned char cCount;
    struct tRTCData testClock;
    
    for (cCount=0; cCount<99; cCount++)
        {
        testClock.minutes = cCount;
        testClock.hours = cCount;
        
        UpdateDigits(&testClock);
        Delay10KTCYx(100);
    }

    return 0;
}

unsigned char CompareOldNew (struct tRTCData * CON_oldRTCData, struct tRTCData * CON_newRTCData)
    {

    unsigned char rc;

    // Determine whether the new time is different from the old time by looking at the minutes
    if (CON_newRTCData->minutes != CON_oldRTCData->minutes)
        {
        rc = 0;
    }
    else
        rc = 1;
        
    return rc;
}

unsigned char UpdateTime(struct tRTCData * UT_oldRTCData, struct tRTCData * UT_newRTCData)
    {
    UT_oldRTCData->minutes = UT_newRTCData->minutes;
    UT_oldRTCData->hours = UT_newRTCData->hours;
}

unsigned char UpdateDigits(struct tRTCData * UD_RTCData)
    {
    // Local variables
    unsigned char tens_of_hours;
    unsigned char units_of_hours;
    unsigned char tens_of_mins;
    unsigned char units_of_mins;
    
    // Convert BCD hours and minutes into digit values
    //** -- might have to change the mod and div as these are very processor intensive operators -- **//
    //tens_of_hours = UD_RTCData->hours/10;
    //units_of_hours = UD_RTCData->hours%10;
    //tens_of_mins = UD_RTCData->minutes/10;
    //units_of_mins = UD_RTCData->minutes%10;
    
    tens_of_hours = UD_RTCData->hours&0xF0;
    tens_of_hours = tens_of_hours >> 4;
    tens_of_hours = tens_of_hours&0x0F;
    
    units_of_hours = UD_RTCData->hours&0x0F;
    
    tens_of_mins = UD_RTCData->minutes&0xF0;
    tens_of_mins = tens_of_mins >> 4;
    tens_of_mins = tens_of_mins&0x0F;
    
    units_of_mins = UD_RTCData->minutes&0x0F;
    
    // Update the digits with their new values
    PCA9555_Write(minsAddr, 0x02, minsx10[tens_of_mins], minsx1[units_of_mins]);
    PCA9555_Write(hoursAddr, 0x02, hrsx10[tens_of_hours], hrsx1[units_of_hours]);
    
    return 0;
}

unsigned int Read_Tic_Counter (void)
    {
	return ReadTimer0();
}

unsigned char Clear_Tic_Counter (void)
    {
    WriteTimer0(0);
}

unsigned char Update_MSF_Record (struct tMSFData * MSFDataToUpdate, unsigned char bitValue)
    {
    // switch (secondCounter)
        // {
        // case 1 :
            // if Bit A & 0x02
                // {
                //Do Stuff
            // };
            // if Bit A & 0x01
                // {
                //DO different stuff
            // };
//            break;
    Nop(); //  }
}

        

/***********************************************************
 ***********************          **************************
 **********************    Main    *************************
 ***********************          **************************
 ***********************************************************/

void main (void)
    {
    unsigned char ec;
    unsigned char bitData;
	unsigned char cUpdateClock;
    unsigned ticCounter;
    unsigned currentTimerContents;
    
    struct tRTCData newRTCData = {0, 0}; // the time, as freshly recovered from the RTC
    struct tRTCData oldRTCData = {8, 8}; // the current time, most of the time
    struct tRTCData msfRTCData = {0, 0}; // the time, taken from the MSF receiver
    
    struct tMSFData currentMSFData = {FindFirstEdge, 0, 0, 0, 0, 0, 0, 0}; // Data gathered from the MSF
    
    cUpdateClock = 0; // this variable tells us whether to go and get the time from the server and update the RTC
    
    
    /***************************************************************/
    /*              Functions to set up hardware                   */
    /***************************************************************/
    
    /* setup PIC hardware */
    SetupPICHardware ();
    
    SetupPCA9555 ();

    SetupPCF8593 ();

    // ScrollTheDigits ();
    
    /* Set all to 00:00 */
    UpdateDigits(&oldRTCData);
    
    /*****************************************************************/
    /* Start the state machine to get the time from the msf receiver */
    /*****************************************************************/
    /*                                                               */
    /* The MSF format outputs the current time and date over the     */
    /* period of a minute with a 2-bit frame every second at 0.1Hz   */
    /*                                                               */
    /* ..immediately prior to second 00 is a period of 700ms at '1'  */
    /* Second 00 : 500ms '0' (followed by 500ms '1')                 */
    /* Second 01 : 100ms low, 100ms A-bit, 100ms B-bit, 700ms '1'    */
    /* subsequent seconds are the same, up to second 59              */
    /*                                                               */
    /* A-bit and B-bit are encoded in BCD with a different digit     */
    /* each second to build up the full time and date over 1 minute  */
    /*                                                               */
    /* EVERY SECOND I STILL NEED TO UPDATE THE CLOCK AND THIS WILL   */
    /* BE DONE AT THE 750ms MARK EVERY SECOND                        */
    /*                                                               */
    /*****************************************************************/
    
    while (1)
        {
        if (cUpdateClock == 1) // I'm getting the valid time from the MSF server
            {
            switch (currentMSFData.MSFState)
                {
                case FindFirstEdge : // wait until we get a valid transition from 1 to 0
                    ec = 0;
                    
                    currentMSFData.MSFsecondCounter = 0;
                                        
                    Clear_Tic_Counter();
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while ((msfInput == msfHigh) && (currentTimerContents < msecs(950))) // wait for it to go low, but only wait for 950ms
                        {
                        currentTimerContents = Read_Tic_Counter(); // keep reading
                    }
                                        
                    // on exiting the while, check that it's not because we've timed out
                    if (currentTimerContents > msecs(950))
                        {
                        ec |= ec_timeout;
                    }
                    
                    // msf input must now be low (assuming no timeout, captured in ec), and can only reliably be low for 500ms
                    Clear_Tic_Counter();
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while ((ec == 0) && (msfInput == msfLow) && (currentTimerContents < msecs(550)))  // wait for it to go high, but only wait for 550ms
                        {
                        Delay100TCYx (10); // some debounce
                        currentTimerContents = Read_Tic_Counter(); // keep reading
                    };
                    
                    // on exiting the while check that it's not because we've timed out
                    if (currentTimerContents > msecs(550))
                        {
                        ec |= ec_timeout;
                    }
                    
                    if (ec == 0) // we have found edges
                        {
                        currentMSFData.MSFState = Bit0;
                        currentMSFData.MSFsecondCounter = 0;
                        // ... and we leave this state with the output having just gone high, ready to look for a falling edge on the input
                    }
                    else // we didn't find any transitions on the clock
                        {
                        currentMSFData.MSFState = FindFirstEdge; // next state will be this state...
                        
                        /* Read from RTC */
                        ReadTime(&newRTCData);
                        
                        /* Update new time if required */
                        if (CompareOldNew(&oldRTCData, &newRTCData) == 0)
                            {
                            UpdateTime(&oldRTCData, &newRTCData);
                            Nop();
                            UpdateDigits(&oldRTCData);
                        }
                    }
                break;
                                    
                case Bit0 : // wait until we get a valid bit 0
                    ec = 0;
                    
                    currentMSFData.MSFsecondCounter = 0;
                    
                    // we enter this knowing that the MSF line has just gone high
                    
                    Clear_Tic_Counter();
                    currentTimerContents = Read_Tic_Counter();
                    while ((ec == 0) && (msfInput == msfHigh) && (currentTimerContents < msecs(950))) // wait for a valid falling edge, but only wait for 950ms
                        {
                        Delay100TCYx (1); // some debounce
                        currentTimerContents = Read_Tic_Counter(); 
                    };
                    
                    Nop();

                    // on exiting the while check that it's not because we've timed out                    
                    if (currentTimerContents > msecs(950))
                        {
                        ec |= ec_timeout;
                    }
                    
                    // then reset the counter to time how long it stays low
                    Clear_Tic_Counter();
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while ((ec == 0) && (msfInput == msfLow) && (currentTimerContents < msecs(450))) // wait for it to go low for at least 450ms
                        {
                        Delay100TCYx (1); // wait between reads
                        currentTimerContents = Read_Tic_Counter();
                    }

                    Nop();
                    
                    // on exiting the while check that it's because the counter has got up to 450ms
                    if (ec != 0)
                        {
                        currentMSFData.MSFState = FindFirstEdge;
                    }
                    else if (currentTimerContents > msecs(450)) // then we are successful
                        {
                        currentMSFData.MSFState = BitN; // the next state will be N
                        currentMSFData.MSFsecondCounter = 1;
                    }
                    
                    // last of all, wait for the timer to reach 975ms ... 
                    while (currentTimerContents < msecs(975)) // busy wait until 750ms past the falling edge
                        {
                        Delay100TCYx (1); // wait between reads
                        currentTimerContents = Read_Tic_Counter();
                    }
                    
                    // ... then update the clock
                    ReadTime(&newRTCData); // read the RTC
                    // Update new time
                    if (CompareOldNew(&oldRTCData, &newRTCData) == 0)
                        {
                        UpdateTime(&oldRTCData, &newRTCData);
                        Nop();
                        UpdateDigits(&oldRTCData);
                    }

                    // we leave this state at around 25ms to the start of another second, with the msf line high
                    
                    break;
                    
                case BitN : // all subsequent bits
                    ec = 0;
                    bitData = 0;
                                        
                    // we enter this state knowing that the MSF line has just gone high and we should have around 25ms to wait
                    
                    Clear_Tic_Counter();
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while ((ec == 0) && (msfInput == msfHigh) && (currentTimerContents < msecs(50))) // wait for a valid falling edge, but only wait for 50ms
                        {
                        Delay100TCYx (1); // some debounce
                        currentTimerContents = Read_Tic_Counter();
                    };
                    
                    // on exiting the while check that it's not because we've timed out                    
                    if (currentTimerContents > msecs(50))
                        {
                        ec |= ec_timeout;
                    }
                    
                    // then reset the counter to start the timing through one second's data
                    Clear_Tic_Counter();
                    
                    // count up to 50ms then make sure that the input is still a zero
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while (currentTimerContents < msecs(50))
                        {
                        Delay100TCYx (1); // wait between timer reads
                        currentTimerContents = Read_Tic_Counter();
                    }
                    
                    // check in the middle (50ms) of the first 100ms bit
                    if (msfInput != msfLow)
                        {
                        ec |= ec_bad_data;
                    }
                    
                    // count up to 150ms then sample bit A
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while (currentTimerContents < msecs(150))
                        {
                        Delay100TCYx (1); // wait between timer reads
                        currentTimerContents = Read_Tic_Counter();
                    }
                    
                    // check in the middle (150ms) of the second 100ms bit (A)
                    bitData |= (msfInput << 1);
                    
                    // count up to 250ms then sample bit B
                    currentTimerContents = Read_Tic_Counter(); // first read
                    while (currentTimerContents < msecs(250))
                        {
                        Delay100TCYx (1); // wait between timer reads
                        currentTimerContents = Read_Tic_Counter();
                    }
                    
                    // check in the middle (250ms) of the second 100ms bit (B)
                    bitData |= (msfInput);
                    
                    // update the MSF record with the bits
                    Update_MSF_Record (&currentMSFData, bitData);
                    
                    // check which bit we're on
                    if (currentMSFData.MSFsecondCounter < 59)
                        {
                        currentMSFData.MSFsecondCounter++;
                    }
                    else
                        {
                        currentMSFData.MSFState = Update_RTC;
                    }

                    // last of all, wait for the timer to reach 975ms ... 
                    while (currentTimerContents < msecs(975)) // busy wait until 750ms past the falling edge
                        {
                        Delay100TCYx (1); // wait between reads
                        currentTimerContents = Read_Tic_Counter();
                    }
                    
                    // ... then update the clock
                    ReadTime(&newRTCData); // read the RTC
                    // Update new time
                    if (CompareOldNew(&oldRTCData, &newRTCData) == 0)
                        {
                        UpdateTime(&oldRTCData, &newRTCData);
                        Nop();
                        UpdateDigits(&oldRTCData);
                    }

                    // again we leave this state at around 20ms to the start of another second, with the msf line high
                    break;
                    
                case Update_RTC:
                    // update time inside RTC
                    currentMSFData.MSFState = Bit0;
                    break;
                    
                default:
                    // dunno
                    break;
            }
        }
        else // just use the time from the RTC
            {
            /* Strobe B0 so that we can measure update period */
            PORTBbits.RB0 = '1';
            
            /* Read from RTC */
            ReadTime(&newRTCData);
            
            /* Update new time */
            if (CompareOldNew(&oldRTCData, &newRTCData) == 0)
                {
                UpdateTime(&oldRTCData, &newRTCData);
                Nop();
                UpdateDigits(&oldRTCData);
            }
            
            /* Unstrobe B0 so that we can measure update time */
            PORTBbits.RB0 = '0';
            
            /* Wait for 50ms */
            //** -- sooner or later, I'm going to have to set up a timer for this and put it in an interrupt -- **//
            Delay10KTCYx(40);
        }
    }    
}
