/******************************************************************************
 *
 *                Nixie Clock Include File
 *
 ******************************************************************************
 * FileName:        nixie2620.h 
 * Dependencies:    None
 * Processor:       PIC18
 * Compiler:        C18
 * Company:         Microchip Technology, Inc.
 *
*******************************************************************************
 * Description
 * runs on nixie board rev 1
******************************************************************************/

/*********************************************************************/
/******************* Pin and Register Definitions ********************/
/*********************************************************************/

// I2C addresses

#define hoursAddr 0x40
#define minsAddr 0x42
#define clockAddr 0xA2

// Pin definitions

#define msfInput PORTCbits.RC7

// Shortcut definitions

#define msfHigh 0
#define msfLow  1

#define BitA 0x02
#define BitB 0x01

#define ec_timeout 0x01
#define ec_bad_data 0x02

#define msecs(time_in_ms) time_in_ms*39 // 10000 clock cycles in a millisecond, 256 cycles per timer count

/***************************************************/
/*                Type Definitions                 */
/***************************************************/

struct tRTCData {
    unsigned char minutes;
    unsigned char hours;
};

enum tMSFState { FindFirstEdge, Bit0, BitN, Update_RTC }; 

struct tMSFData {
    enum tMSFState MSFState;
    unsigned char MSFsecondCounter;
    unsigned char MSFminutes;
    unsigned char MSFhours;
    unsigned char MSFdate;
    unsigned char MSFmonth;
    unsigned char MSFyear;
    unsigned char MSFindex;
};

/*********************************************************************/
/*********************** Function Prototypes *************************/
/*********************************************************************/

unsigned char PCA9555_Write (unsigned char, unsigned char, unsigned char, unsigned char);
unsigned char PCF8593_Write (unsigned char, unsigned char, unsigned char);
unsigned char PCF8593_Read (unsigned char, struct tRTCData *, unsigned char);
unsigned char BlankDisplay(unsigned char);
unsigned char SetupI2CDevices(void);
unsigned char SetupPICHardware (void);
unsigned char ScrollTheDigits (void);
unsigned char Clear_Tic_Counter (void);
unsigned Read_Tic_Counter (void);
unsigned char UpdateDigits(struct tRTCData *);
unsigned char CompareOldNew (struct tRTCData *, struct tRTCData *);
