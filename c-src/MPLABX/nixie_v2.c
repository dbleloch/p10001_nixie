/******************************************************************************
 *
 *                Nixie Clock Project
 *
 ******************************************************************************
 * FileName:        nixie_v1.c 
 * Dependencies:    None
 * Processor:       PIC18F2620
 * Compiler:        C18
*******************************************************************************
 * Description
 * i2c bus expander for controlling the nixie elements
 * i2c real time clock
 * future stuff must include i/o to set the clock functions et
 c.
******************************************************************************/

#include <p18f2620.h>
#include <nixie2620.h>
#include <delays.h>
#include <i2c.h>

// #include <sw_uart.h>
// #include <xlcd.h>
// #include <float.h>
// #include <string.h>

/******************************************************************
 *
 *   Sys clock, baud rate and timeout calculations
 *
 * Incoming clock frequency 10MHz; Oscillator period = 100ns
 * x4 PLL enabled to generate 40MHz internal clock
 * Instruction Cycle (4 clk/cycle) TCy 125ns
 *
 * Delay10TCy = 1.25us
 * Delay100TCy = 12.5us
 * Delay1kTCy = 125us
 * Delay10kTCy = 1.25ms
 * Delay10kTCy(40) = 50ms
 * Delay10kTCy(200) = 250ms
 *
 ******************************************************************/

// #pragma config OSC = HSPLL
// #pragma config WDT = OFF
// #pragma config LVP = OFF
// #pragma config DEBUG = ON

// 
// #define ENABLE_LCD 0xFF
// // #define ENABLE_LCD 0x00
// 
// #define ENABLE_BUSY_WAIT_ON_WRITE 0
// 
// /******************************************************************************/
// /*                    Command list for the VMusic2 card                       */
// /******************************************************************************/
// 
// #define NUMCMDS 13
// 
// #define VM2_CR 0x01
// #define VM2_STOP 0x01               
// #define VM2_PLAYALL 0x02            
// #define VM2_PLAYALLREPEAT 0x03      
// #define VM2_PLAYALLRANDOMREPEAT 0x04
// #define VM2_NEXT 0x05               
// #define VM2_PREV 0x06               
// #define VM2_NEXTALBUM 0x07          
// #define VM2_FWD5SECS 0x08           
// #define VM2_RWD5SECS 0x09            
// #define VM2_PAUSE 0x0A              
// #define VM2_SETVOL 0x0B             
// #define VM2_ECHO 0x0C               
// 
// const rom unsigned char * numeral6 = "6";
// 
// const rom unsigned char * vm2CMD[NUMCMDS] = {"\r",
//                                              "VST\r",
//                                              "V3A\r", 
//                                              "VRA\r", 
//                                              "VRR\r", 
//                                              "VSF\r", 
//                                              "VSB\r", 
//                                              "VSD\r", 
//                                              "VF\r", 
//                                              "VB\r", 
//                                              "VP\r", 
//                                              "VSV ", 
//                                              "E\r"};
// 
//  
// 
// /******************************************************************************/
// /*                     Received frame list for the iBus                       */
// /******************************************************************************/
// 
// #define NUMRXCODES 17
// 
// #define iB_MFD_VOL_UP 0x00
// #define iB_MFD_VOL_DN 0x01
// #define iB_MFD_NEXT 0x02
// #define iB_MFD_NEXT_REL 0x03
// #define iB_MFD_PREV 0x04
// #define iB_MFD_PREV_REL 0x05
// #define iB_RAD_POLL_CHG 0x06
// #define iB_RAD_CURRENT_CHG_STATE 0x07
// #define iB_RAD_CHR_STOP 0x08
// #define iB_RAD_CHR_START 0x09
// #define iB_RAD_CHR_SCAN_FWD 0x0A
// #define iB_RAD_CHR_SCAN_REV 0x0B
// #define iB_RAD_CHR_GOTO_CD 0x0C
// #define iB_RAD_CHR_NEXT_TRACK 0x0D
// #define iB_RAD_CHR_PREV_TRACK 0x0E
// #define iB_RAD_CHR_SHUF_ON 0x0F
// #define iB_RAD_CHR_SHUF_OFF 0x10
//  
// const rom struct constantFrame knownRxFrames[NUMRXCODES] = {
// 
// /*   Sour  Len   Dest  D0    D1    D2    D3    D4    D5    D6    D7      */
//     {0x50, 0x04, 0x68, 0x32, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Volume up
//     {0x50, 0x04, 0x68, 0x32, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Volume down
//     {0x50, 0x04, 0x68, 0x3B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Next
//     {0x50, 0x04, 0x68, 0x3B, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Next release
//     {0x50, 0x04, 0x68, 0x3B, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Prev
//     {0x50, 0x04, 0x68, 0x3B, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Prev release
//     {0x68, 0x03, 0x18, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio poll for changer
//     {0x68, 0x05, 0x18, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio ask changer for current CD & track ID
//     {0x68, 0x05, 0x18, 0x38, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to stop playing
//     {0x68, 0x05, 0x18, 0x38, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to start playing
//     {0x68, 0x05, 0x18, 0x38, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to fast scan forward
//     {0x68, 0x05, 0x18, 0x38, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to fast scan backward
//     {0x68, 0x05, 0x18, 0x38, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to go to CD# (D2)
//     {0x68, 0x05, 0x18, 0x38, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to next track
//     {0x68, 0x05, 0x18, 0x38, 0x0A, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to prev track
//     {0x68, 0x05, 0x18, 0x38, 0x08, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00},  // Radio asks CD to go to shuffle mode on
//     {0x68, 0x05, 0x18, 0x38, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}  // Radio asks CD to go to shuffle mode off
// };
 
const rom char minsx10[10] = {0xB7,  // 0
                              0x7B,  // 1
                              0x5F,  // 2
                              0xAF,  // 3
                              0xBB,  // 4
                              0x6F, // 5
                              0x77,  // 6
                              0x7D,  // 7 // U6
                              0x9F,  // 8
                              0xBD};  // 9
                             
const rom char minsx1[10] = {0xBE,  // 0 // U7
                             0xBD,  // 1
                             0xF6,  // 2
                             0xED,  // 3
                             0xF5,  // 4
                             0xF9, // 5
                             0xFA,  // 6
                             0xDD,  // 7
                             0xEE,  // 8
                             0xDE};  // 9
                             
const rom char hrsx10[10] = {0x7D,  // 0 // U8
                             0x7B,  // 1
                             0x77,  // 2
                             0x6F,  // 3
                             0x5F,  // 4
                             0xBD,  // 5
                             0xBB,  // 6
                             0xB7,  // 7
                             0xAF,  // 8
                             0x9F}; // 9

const rom char hrsx1[10] = {0xF9,  // 0 // U9
                              0xF5,  // 1
                              0xED,  // 2
                              0xDD,  // 3
                              0xBD,  // 4
                              0xFA,  // 5
                              0xF6,  // 6
                              0xEE,  // 7
                              0xDE,  // 8
                              0xBE}; // 9

struct tRTCData * newRTCData = {0, 0};

struct tRTCData * oldRTCData = {0, 0};
                            
                              
// const rom unsigned char * LCD_TXT[NUMRXCODES] = {"MFD Volume up   ", 
//                                                  "MFD Volume down ",  
//                                                  "MFD Next        ",
//                                                  "MFD Next rel    ", 
//                                                  "MFD Prev        ",
//                                                  "MFD Prev rel    ",
//                                                  "Poll Changer    ",
//                                                  "Request Status  ",
//                                                  "Stop            ",
//                                                  "Start           ",
//                                                  "Scan Forward    ",
//                                                  "Scan Reverse    ",
//                                                  "Goto CD #       ",
//                                                  "Next Track      ",
//                                                  "Prev Track      ",
//                                                  "Shuffle On      ",
//                                                  "Shuffle Off     "};
// 
// 

/******************************************************************************/


/****************************************************************
 ***********************               **************************
 **********************    Functions    *************************
 ***********************               **************************
 ****************************************************************/
 
 
/* setup PIC hardware */
unsigned char SetupPICHardware (void)
    {
    ADCON1 = 0b00001111;   // ADCs off
    ADCON0 = 0b00000000;   // ADCs off

    TRISA = 0b11000000; // switch is an input
    TRISB = 0b00000000;
    TRISC = 0b10100000;
//    LEDs_TRIS = 0;   // LEDs are outputs
        
    //  Count the time between bytes
    /*  bit 7 : TMR0ON = 1 : Timer on            *
     *  bit 6 : T08BIT = 0 : 16 bit              *
     *  bit 5 : T0CS = 0 :  Internal clk         *
     *  bit 4 : T0SE = 0 :  Count on rising edge *
     *  bit 3 : PSA = 0  :  Prescaler used       *
     *  bit 2 : T0PS2 = 0 :                      *
     *  bit 1 : T0PS1 = 1 : 8x prescaler         *
     *  bit 0 : T0PS0 = 0 :                      */
    T0CON = 0b00000000;
    
    // Open I2C Peripheral
    OpenI2C(MASTER, SLEW_OFF);
    
    // Set 100kHz Baud Rate for a 40MHz clock
    SSPCON1 = 0b00111000;
    SSPCON2 = 0b00011000;
    SSPADD =  24; // Baud rate = fosc / (4* SSPADD+1)
    
    return 0;
}

unsigned char SetupI2CDevices(void)
    {
    
    /* Reset RTC */
    PORTCbits.RC2 = 0;
    
    /* Configure minutes driver */
    PCA9555_Write(minsAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    /* Configure hours driver */
    PCA9555_Write(hoursAddr, 0x06, 0x00, 0x00);
    Delay10KTCYx(10);
    
    /* Bring RTC out of reset */
    PORTCbits.RC2 = 0;
    Delay10KTCYx(10);
    
    /* Configure RTC & hold */
    PCF8593_Write(clockAddr, 0x00, 0x80);
    Delay10KTCYx(10);
    
    /* Load time into RTC & start */
    PCF8593_Write(clockAddr, 0x04, 0x80); // hours
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x03, 0x80); // mins
    Delay10KTCYx(10);
    
    PCF8593_Write(clockAddr, 0x00, 0x00); // start counting
    Delay10KTCYx(10);
}

unsigned char BlankDisplay(unsigned char = 0x01)
    {
    nop();
}

/* Write to the port expander */
unsigned char PCA9555_Write (unsigned char addr, unsigned char command, unsigned char byte0, unsigned char byte1)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    WriteI2C(byte1);
    StopI2C();
    return 0;
}

/* Write to the rtc */
unsigned char PCF8593_Write (unsigned char addr, unsigned char command, unsigned char byte0)
    {
    StartI2C();
    WriteI2C(addr);
    WriteI2C(command);
    WriteI2C(byte0);
    StopI2C();
    return 0;
}

unsigned char PCF8593_Read (unsigned char addr, unsigned char * rtcData)
    {
    StartI2C();
    
    StopI2C();
    return 0;
}

unsigned char ScrollTheDigits (void)
    {
    for (cCount=0; cCount=9; cCount++)
        {
        PCA9555_Write(minsAddr, 0x02, minsx10[cCount], minsx1[cCount]);
        PCA9555_Write(hoursAddr, 0x02, hoursx10[cCount], hoursx1[cCount]);
        Delay10KTCYx(200);
    }
}

unsigned char CompareOldNew (struct tRTCData * oldRTCData, struct tRTCData * newRTCData)
    {
     
}

unsigned char UpdateTime(struct tRTCData * oldRTCData, struct tRTCData * newRTCData)
    {
     
}

// unsigned int read_timeout_timer (void)
//     {
//     
//     unsigned int iTimerLow;
//     unsigned int iTimerHigh;
// 
//     iTimerLow = TMR0L;
//     iTimerHigh = TMR0H;
// 
//     iTimerLow &= 0x00FF;  // Clear all the high bits in the low byte timer
// 
//     iTimerHigh *= 256;      // Shift high byte to top 8 bits
// 
//     iTimerHigh |= iTimerLow;  // Or in the low bits for the full timer reading
// 
//     return iTimerHigh;   
// 
// }


// void clear_timeout_timer (void)
//     {
//     
//     TMR0H = 0;
//     TMR0L = 0;
// 
// }


// void setup_iBus_UART_hardware (void)
//     {
// 
//     /* UART transmit pin */
//     iB_TX_TRIS = 0;
// 
//     /* UART receive pin */
//     iB_RX_TRIS = 1;
// 
//     /* UART sniffer pin */
//     iB_Sniffer_TRIS = 1;
// 
//     /* Set output line high (this means no FET on, iBus line high) ... circuit must be positive logic... low on TX pin = iBus line pulled low */
//     iB_TX_LAT = 1; 
// 
//     //    SPBRG = 25; // Baud rate set for 9600 @ 4MHz
//     //    SPBRG = 103; // Baud rate set for 9600 @ 16MHz
//     SPBRG = 207; // Baud rate set for 9600 @ 32MHz
// 
//     /*  AUSART Transmit Register                 *
//      *  bit 7 : CSRC = 0 : Don't care            *
//      *  bit 6 : TX9 = 1 : 9bit transmission      *
//      *  bit 5 : TXEN = 1 : Transmit ENable      *
//      *  bit 4 : SYNC = 0 : Asynchronous mode     *
//      *  bit 3 : Unimplemented bit = 0            *
//      *  bit 2 : BRGH = 1 : High speed mode       *
//      *  bit 1 : TRMT = 1 : Transmit shift reg    *
//      *  bit 0 : TX9D = 0 : Parity bit            */
//     TXSTA = 0b01100110;
// 
//     /* AUSART Receive Register                   *
//      *  bit 7 : SPEN = 1 : Serial port enabled   *
//      *  bit 6 : RX9 = 1 : 9bit reception         *
//      *  bit 5 : SREN = 0 : Don't care            *
//      *  bit 4 : CREN = 1 : Enable receiver       *
//      *  bit 3 : ADDEN = 0 : 9th bit for parity   *
//      *  bit 2 : FERR = 0 : No framing error      *
//      *  bit 1 : OERR = 0 : No overrun error      *
//      *  bit 0 : RX9D = 0 : 9th data bit          */
//     RCSTA = 0b11010000;
// 
//     /*  BAUDCON Baud Rate Control Reg            *
//      *  bit 7 : ABDOVF = 0 : Clear rollover      *
//      *  bit 6 : RCIDL = 0 : Read only            *
//      *  bit 5 : Unimplemented bit = 0            *
//      *  bit 4 : SCKP = 0 : Asynchronous mode     *
//      *  bit 3 : BRG16 = 0 : 8 bit BRG mode       *
//      *  bit 2 : Unimplemented bit = 0            *
//      *  bit 1 : WUE = 0 : Rx wake up disabled    *
//      *  bit 0 : ABDEN = 0 : No auto baud rate    */
//     BAUDCON = 0b00000000; 
// 
// }


/* Setup Vmusic UART */
// void setup_Vm_UART_hardware (struct CDchanger * CDPlayer)
//     {
// 
//     /* UART transmit pin */
//     VM_UART_TX_TRIS = 0;
//     
//     /* UART receive pin */
//     VM_UART_RX_TRIS = 1;
//     
//     /* Set output line high (then it gets pulled low when a start bit occurs) */
//     VM_UART_TX_LAT = 1;
//         
//     // OpenUART();
//     
//     CDPlayer->status = CD_IDLE;
//     CDPlayer->current_track = 0;
//     CDPlayer->current_disk = 0;
//     
// }


// unsigned char VM_UART_write(unsigned char cmdID)
//     {
//     unsigned char string_pointer_count;
//     unsigned char stringy[14];
// 
//     // copy string required from program memory to data memory
//     strcpypgm2ram (stringy, vm2CMD[cmdID]);
// 
//     // serialize out the string to the UART
//     for (string_pointer_count=0; stringy[string_pointer_count] != '\0'; ++string_pointer_count)
//         {
//         WriteUART(stringy[string_pointer_count]);
//         Delay10TCYx(300);
//     }
// }


// void init_Vmusic (void)
//     {
//     VM_UART_write(VM2_PLAYALLRANDOMREPEAT);
// }


// void setup_LCD_hardware (void)
//     {
//         
//     OpenXLCD(FOUR_BIT&LINES_5X7);        // Init the LCD Display
//     while (BusyXLCD());
//     SetDDRamAddr(0);
//     while (BusyXLCD());
//     putrsXLCD ("Waiting :       ");
// }

 
// void DelayFor18TCY(void)
//     {
//     Delay10TCYx(0x2);    //delays 20 cycles
//     return;
// }


// void DelayPORXLCD(void)   // minimum 15ms
//     {
//     Delay1KTCYx(120);      // 1000*120*125ns
//     return;
// }


// void DelayXLCD(void)     // minimum 5ms
//     {
//     Delay1KTCYx(0x40);      // 1kTCY * 40 * 125ns
// }


// unsigned char * htoa(unsigned char byte, unsigned char *string)
//     {
//     unsigned char lnibb;
//     unsigned char unibb;
//     
//     lnibb = byte & 0b00001111;
//     unibb = (byte & 0b11110000)>>4;
//     if (unibb < 9)
//         {
//         string[0] = unibb + 48;
//     }
//     else
//         {
//         string[0] = unibb + 55;
//     }
//     if (lnibb < 9)
//         {
//         string[1] = lnibb + 48;
//     }
//     else
//         {
//         string[1] = lnibb + 55;
//     }
//     string[2] = '\0';
//     return string;
// }


// unsigned char calcCRC(struct iBusFrame * aframe)
//     {
//     unsigned char crcCount;
//     unsigned char crcResult;
//     
//     crcResult = 0;
//     crcResult ^= aframe->source;
//     crcResult ^= aframe->length;
//     crcResult ^= aframe->dest;
//     for (crcCount = 0; crcCount <= ((aframe->length)-2); crcCount++)
//         {
//         crcResult ^= aframe->data[crcCount];
//     }
//     aframe->crc = crcResult;
//     return crcResult;
// }


// unsigned char calcParity(unsigned char iBusByte)
//     {
//     unsigned char parCount = 0;
//     unsigned char parResult = 0;
//     
//     for (parCount = 0; parCount <= 8; parCount++)
//         {
//         if ((iBusByte & 0x01) == 0x01)
//             {
//             parResult ^= 0x0001;
//         }
//         iBusByte >> 1;
//     }
//     return parResult;
// }


// void wait_iBus_clear_for_15ms(void)
//     {
//     unsigned char cTicCounter;
// 
//     for (cTicCounter = 0; cTicCounter < 128; cTicCounter++)
//         {
//         //Check status of line every tic :
//         //Where one Tic is = 117us (15ms / 128counts)
//         //117us = 940 machine cycles @ 125ns
//         Delay10TCYx(94);
//         if (iBusSniffer == 1) cTicCounter = 0;
//     }
// }


// unsigned char busy_wait_check_iBus_data(unsigned char cCheckMeByte, unsigned char cParity)
//     {
//     unsigned char cBitCounter;
//     unsigned char cCheckBits = 0;
//     
//     /********** START-1-2-3-4-5-6-7-8-PARITY-STOP **********/
//     
//     Delay10TCYx(54); // delay to get into the middle of the start bit
//     
//     /********** START **********/
//     LEDs = 0xFF;
//     if (iBusSniffer == 0) cCheckBits |= 1;
//     LEDs = 0x00;
//     Delay10TCYx(80); // delay to get into the middle of the next bit
//     
//     /********** BITs 1-8 **********/
//     for (cBitCounter = 0; cBitCounter < 8; cBitCounter++)
//         /* START-1-2-3-4-5-6-7-8-PARITY-STOP */
//         {
//         LEDs = 0xFF;
//         if (iBusSniffer == (cCheckMeByte & 0x01)) cCheckBits |= 2;
//         cCheckMeByte<<1;
//         LEDs = 0x00;
//         Delay10TCYx(81); // delay to get into the middle of the next bit
//     }
//     
//     /********** PARITY **********/
//     LEDs = 0xFF;
//     if (iBusSniffer == cParity) cCheckBits |= 4;
//     LEDs = 0x00;
//     Delay10TCYx(80); // delay to get into the middle of the next bit
//     
//     /********** STOP **********/
//     LEDs = 0xFF;
//     if (iBusSniffer == 1) cCheckBits |= 8;
//     LEDs = 0x00;
//         
//     return CODE_VALID;
// }


// unsigned char write_iBus(struct iBusFrame * writeFrame)
//     {
//     
//     char * string;
//     
//     /* Error code */
//     unsigned char rc;
// 
//     /* Temporary parity storage */
//     unsigned char tempParity;
//     
//     /* On-the-fly calculation of CRC from incoming data */
//     unsigned char iBusCalcCRC;
//     
//     /* Counter... not used here */
//     unsigned int inFrameCount[6];
//     
//     /* Timeout... not used */
//     unsigned int betweenFrameCount;
//     
//     /* Timeout between bytes of one frame */
//     unsigned int iTimeOutCounter;
//     
//     /* Not used */
//     unsigned char iFrameTimeCounter;
//     
//     /* noTimer ... debug for timer investigation ... not used */
//     unsigned int noTimer = 0xFFFF;
//     
//     /* Data holder for copying incoming data into memory */
//     unsigned char tempIBusData;
//     
//     /* Counts number of data bytes in frame */
//     unsigned char iBusDataCounter;
//      
//     /* iBus state machine
//      *      state 0 : sending first byte ... destination
//      *      state 1 : sending second byte ... length - timeout enabled
//      *      state 2 : sending third byte ... destination - timeout enabled
//      *      state 3 : sending 4th+ bytes ... data - timeout enabled
//      *      state 4 : sending last byte ... CRC - timeout enabled */
//     unsigned char iBusState = 0;
//     
//     do
//         {
//         switch (iBusState)
//             {
//             case 0 : // Send the source byte
//                 LEDs = 0x00;
//                 writeFrame->status = 0;
//     
//                 tempParity = calcParity(writeFrame->source);
//                 while (TXSTAbits.TRMT == 0) ;
//                 TXSTAbits.TX9D = tempParity;
//                 TXREG = writeFrame->source;
//                 
//                 /* check each bit as it goes out */
//                 if (busy_wait_check_iBus_data(writeFrame->source, tempParity) == CODE_INVALID)
//                     {
//                     iBusState = 0x00; // go to the next state (get frame length)
//                     rc = CODE_INVALID;
//                 }
//                 else
//                     {
//                     iBusState = 0x01; // go to the next state (get frame length)
//                 }
//                 break;
//             
//             case 1 : // Send the frame length byte
//                 LEDs = 0x01;
//                 
//                 tempParity = calcParity(writeFrame->length);
//                 while (TXSTAbits.TRMT == 0) ;
//                 TXSTAbits.TX9D = tempParity;
//                 TXREG = writeFrame->length;
//                 
//                 /* check each bit as it goes out */
//                 if (busy_wait_check_iBus_data(writeFrame->length, tempParity) == CODE_INVALID)
//                     {
//                     iBusState = 0x00; // go to the next state (get frame length)
//                     rc = CODE_INVALID;
//                 }
//                 else
//                     {
//                     iBusState = 0x02; // go to the next state (get frame length)
//                 }
//                 break;
//                 
//             case 2 : // Send the dest byte
//             
//                 LEDs = 0x02;
//                 
//                 tempParity = calcParity(writeFrame->dest);
//                 while (TXSTAbits.TRMT == 0) ;
//                 TXSTAbits.TX9D = tempParity;
//                 TXREG = writeFrame->dest;
//                 
//                 /* check each bit as it goes out */
//                 if (busy_wait_check_iBus_data(writeFrame->dest, tempParity) == CODE_INVALID)
//                     {
//                     iBusState = 0x00; // go to the next state (get frame length)
//                     rc = CODE_INVALID;
//                 }
//                 else
//                     /* can we skip data */
//                     {
//                     if (writeFrame->length > 2) // is there ANY data (other than src and crc)
//                         {
//                         iBusState = 0x03; // get some data in 
//                     }
//                     else
//                         {
//                         iBusState = 0x04; // go straight to the CRC
//                     }
//                 }
//                 break;
//              
//             case 3 : // Send the data
//                 LEDs = 0x03;
//                 
//                 for (iBusDataCounter = 0; iBusDataCounter < ((writeFrame->length)-2); iBusDataCounter++)
//                     {
//                     tempParity = calcParity(writeFrame->data[iBusDataCounter]);
//                     while (TXSTAbits.TRMT == 0) ;
//                     TXSTAbits.TX9D = tempParity;
//                     TXREG = writeFrame->data[iBusDataCounter];
//                 
//                     if (busy_wait_check_iBus_data(writeFrame->data[iBusDataCounter], tempParity) == CODE_INVALID)
//                         {
//                         iBusState = 0x00; // go to the next state (get frame length)
//                         rc = CODE_INVALID;
//                     }
//                     else
//                         {
//                         iBusState = 0x04; // go to the next state (get frame length)
//                     }
//                 }
//                 break;
//             
//             case 4 : // Get the CRC byte
//                 LEDs = 0x04;
//                 
//                 tempParity = calcParity(writeFrame->crc);
//                 while (TXSTAbits.TRMT == 0) ;
//                 TXSTAbits.TX9D = tempParity;
//                 TXREG = writeFrame->crc;
//                                 
//                 /* check each bit as it goes out */
//                 if (busy_wait_check_iBus_data(writeFrame->crc, tempParity) == CODE_INVALID)
//                     {
//                     iBusState = 0x05; // go to the next state (get frame length)
//                     rc = CODE_INVALID;
//                 }
//                 else
//                     {
//                     rc = CODE_VALID;
//                     iBusState = 0;
//                 }
//                 break;
//                 
//             case 5 : // Get the CRC byte
//                 LEDs = 0x05;
//                 //DelayTXBitUART();  // Delay of one bit at the end of the frame of Dela
//                 Nop();
//                 iBusState = 0x00; // go to the next state (get frame length)
//                 break;
//         }
//     }
//     while (iBusState != 0); /* completes the 'do' around the switch statement */
//     return rc;
// }


// struct iBusFrame * fillFrameFromConstant(struct iBusFrame * frameToFill, unsigned char frameID)
//     {
//     unsigned char cDataCounter = 0;
//     unsigned char tempCRC = 0;
//     
//     memcpypgm2ram (frameToFill, &(TxFrames[frameID]), 0x24);
//     
//     return frameToFill;
// }


// unsigned char matchFrame(struct iBusFrame * recvFrame)
//     {
// 
//     unsigned char cMatchedFrameID;   // use to detect match, then store match ID
//     unsigned char cMatchCounter;     // count known frames
//     unsigned char cDataCounter;      // count data
//     
//     struct iBusFrame checkFrame;
//     
//     cMatchCounter = NUMRXCODES-1;
//     do
//         {
// 
//         // Fill check frame with a known frame
//         memcpypgm2ram (&checkFrame, &(knownRxFrames[cMatchCounter]), 0x24);
//         
//         // Store the ID that we're checking
//         cMatchedFrameID = cMatchCounter;
// 
//         if (checkFrame.source != recvFrame->source) cMatchedFrameID = 0;
//         if (checkFrame.length != recvFrame->length) cMatchedFrameID = 0;
//         if (checkFrame.dest != recvFrame->dest) cMatchedFrameID = 0;
//         cDataCounter = ((recvFrame->length) - 2) ;  // Set DataCounter to the number of data bytes we have
//         while (cDataCounter > 0)
//             {
//             if (checkFrame.data[cDataCounter-1] != recvFrame->data[cDataCounter-1]) cMatchedFrameID = 0;
//             cDataCounter--;
//         }
//         cMatchCounter--;
//     } while ((cMatchCounter > 0) && (cMatchedFrameID == 0));
// 
//     return cMatchedFrameID;
// }


// struct iBusFrame* read_iBus(struct iBusFrame * readFrame)
//     {
//     /* Error return codes */
//     unsigned char rc;
//     
//     unsigned char *string; //= "12";
//     
//     /* On-the-fly calculation of CRC from incoming data */
//     unsigned char iBusCalcCRC;
//     
//     /* Timeout... not used */
//     //unsigned int iBetweenFrameCount;
//     
//     /* Timeout between bytes of one frame */
//     unsigned int iTimeOutCounter;
//     
//     /* Not used */
//     unsigned int iFrameTimeCounter;
//     
//     /* Data holder for copying incoming data into memory */
//     unsigned char cTempIBusData;
//     
//     /* Counts number of data bytes in frame */
//     unsigned char iBusDataCounter;
//     
//     /* iBus state machine
//      *      state 0 : waiting for first byte ... destination
//      *      state 1 : waiting for second byte ... length - timeout enabled
//      *      state 2 : waiting for third byte ... destination - timeout enabled
//      *      state 3 : waiting for 4th+ bytes ... data - timeout enabled
//      *      state 4 : waiting for last byte ... CRC - timeout enabled */
//     unsigned char iBusState = 0;
//         
//     do
//         {
//         switch (iBusState)
//             {
//             case 0 : // Get the source byte
//                 LEDs = 0x00;
//                 readFrame->status = CODE_INVALID;
//                 iBusDataCounter = 0; // reset number of data bytes in frame
//                 while (BusyXLCD());
//                 SetDDRamAddr(0xC0);
//                 while (BusyXLCD());
//                 //putrsXLCD ("                ");
//                 
//                 if (PIR1bits.RCIF == 1) // new data?
//                     {
//                     readFrame->source = RCREG; // get the input byte
//                     clear_timeout_timer(); // reset timeout counter
//                     iBusCalcCRC = readFrame->source; // start the CRC with the first byte
//                     iBusState = 0x01; // go to the next state (get frame length)
//                     if (readFrame->source == 0x68)
//                         {
//                         while (BusyXLCD());
//                         putsXLCD (htoa(readFrame->source, string)); // put the code to the LCD
//                     }
//                 }
//                 break;
//             
//             case 1 : // Get the frame length byte
//                 LEDs = 0x01;
//                 iTimeOutCounter = read_timeout_timer();
//                 if (iTimeOutCounter >= IN_FRAME_TIMEOUT) // is the second byte more than 11 bits delayed, if so then it's the start of another frame
//                     {
//                     rc = iBusState;
//                     iBusState = 0;
//                     readFrame->status |= 0x02;
//                     break;
//                 }
//                 else if (PIR1bits.RCIF == 1) // new data?
//                     {
//                     readFrame->length = RCREG;
//                     clear_timeout_timer(); // reset timeout counter
//                     iBusCalcCRC ^= readFrame->length;
//                     iBusState = 0x02;
//                     if (readFrame->length == 0x03)
//                         {
//                         while (BusyXLCD());
//                         putsXLCD (htoa(readFrame->length, string)); // put the code to the LCD
//                     }
//                 }
//                 break;
//             
//             case 2 : // Get the dest byte
//                 LEDs = 0x02;
//                 iTimeOutCounter = read_timeout_timer();
//                 if (iTimeOutCounter >= IN_FRAME_TIMEOUT) // is the second byte more than 11 bits delayed, if so then it's the start of another frame
//                     {
//                     rc = iBusState;
//                     iBusState = 0;
//                     readFrame->status |= 0x02;
//                     break;
//                 }
//                 else if (PIR1bits.RCIF == 1) // new data?
//                     {
//                     readFrame->dest = RCREG;
//                     clear_timeout_timer(); // reset timeout counter
//                     iBusCalcCRC ^= readFrame->dest;
//                     if (readFrame->length > 2) // is there ANY data (other than src and crc)
//                         {
//                         iBusState = 0x03; // get some data in 
//                     }
//                     else
//                         {
//                         iBusState = 0x04; // go straight to the CRC
//                     }
//                     if (readFrame->length == 0x18)
//                         {
//                         while (BusyXLCD());
//                         putsXLCD (htoa(readFrame->dest, string)); // put the code to the LCD
//                     }
//                 }
//                 break;
//             
//             case 3 : // Get in the data
//                 LEDs = 0x03;
//                 iTimeOutCounter = read_timeout_timer();
//                 if (iTimeOutCounter >= IN_FRAME_TIMEOUT) // is the second byte more than 11 bits delayed, if so then it's the start of another frame
//                     {
//                     rc = iBusState;
//                     iBusState = 0;
//                     readFrame->status |= 0x02;
//                     break;
//                 }
//                 else if (PIR1bits.RCIF == 1) // new data?
//                     {
//                     cTempIBusData = RCREG;
//                     clear_timeout_timer(); // reset timeout counter
//                     if (iBusDataCounter < 0x20)
//                         {
//                         readFrame->data[iBusDataCounter] = cTempIBusData;
//                     }
//                     iBusCalcCRC ^= cTempIBusData; // calc CRC
//                     iBusDataCounter++;
//                     if (iBusDataCounter >= (readFrame->length - 2)) // check against frame length
//                         {
//                         iBusState = 0x04;
//                     }
//                     if (iBusDataCounter <= 3) // print up to the first 4 data bytes
//                         {
//                         if (readFrame->length == 0x01)
//                             {
//                             while (BusyXLCD());
//                             putsXLCD (htoa(readFrame->data[iBusDataCounter-1], string)); // put the code to the LCD
//                         }
//                     }
//                 }
//                 break;
//             
//             case 4 : // Get the CRC byte
//                 LEDs = 0x04;
//                 iTimeOutCounter = read_timeout_timer();
//                 if (iTimeOutCounter >= IN_FRAME_TIMEOUT) // is the second byte more than 11 bits delayed, if so then it's the start of another frame
//                     {
//                     rc = iBusState;
//                     readFrame->status |= 0x02;
//                     break;
//                 }
//                 else if (PIR1bits.RCIF == 1) // new data?
//                     {
//                     readFrame->crc = RCREG;
//                     clear_timeout_timer(); // reset timeout counter
//                     if ((readFrame->crc ^ iBusCalcCRC) == 0)
//                         {
//                         readFrame->status = CODE_VALID;
//                     }
//                     else
//                         {
//                         // readFrame->status |= 0x04; // mark the frame as CRC no match
//                     }
//                     iBusState = 0;
//                     //if (readFrame->length == 0x72)
//                     //    {
//                         while (BusyXLCD());
//                         putsXLCD (htoa(readFrame->crc, string)); // put the code to the LCD
//                     //}
//                 }
//                 break;
//         }
//     }
//     while (iBusState != 0); /* completes the 'do' around the switch statement */
//     
//     if ((readFrame->status & 0x02) == 0x02)
//         {
//         SetDDRamAddr(0xC0);
//         while (BusyXLCD());
//         putrsXLCD ("Fail ");
//         while (BusyXLCD());
//         putsXLCD (htoa(rc, string));
//     }
//     
//     return readFrame;
// }


// void display_Text( unsigned char cFrameID )     
//     {
//     
//     unsigned char *string; //= "12";
//     unsigned char stringy[15];
// 
//     // copy string required from program memory to data memory
//     strcpypgm2ram (stringy, LCD_TXT[cFrameID]);
// 
//     while (BusyXLCD());
//     SetDDRamAddr(0xC0);
// 
//     while (BusyXLCD());
//     putsXLCD (stringy);
//     
// }


// void clear_Text (unsigned char line)
//     {
//     if ((line & 0x01) == 0x01)
//         {
//         while (BusyXLCD());
//         SetDDRamAddr(0x00);
//         while (BusyXLCD());
//         putrsXLCD ("               ");
//     }
//     if ((line & 0x02) == 0x02)
//         {
//         while (BusyXLCD());
//         SetDDRamAddr(0xC0);
//         while (BusyXLCD());
//         putrsXLCD ("               ");
//     }
// }


/***********************************************************
 ***********************          **************************
 **********************    Main    *************************
 ***********************          **************************
 ***********************************************************/

void main (void)
    {
    
    // This is the main working iBus frame
    // struct iBusFrame workingFrame;

    // This is the CD changer emulator structure
    // struct CDchanger CDPlayer1;
    
    // Temporary storage for CRC function call
    // unsigned char cTempCRC;
    
    // Matched iBus Frame ID byte
    // unsigned char cMatchedFrameID;
    
    /* used for XLCD GUI - not used */
    // unsigned int adcResult = 0x0000;
    
    /***************************************************************/
    /*              Functions to set up hardware                   */
    /***************************************************************/
    
    /* setup PIC hardware */
    SetupPICHardware ();
    
    SetupI2CDevices ();

    ScrollTheDigits ();
    
    while (1)
        {
        /* Read from RTC */
        ReadTime(newRTCData);

        /* Update new time */
        if (CompareOldNew(oldRTCData, newRTCData) == 0)
	    {
            UpdateTime(oldRTCData, newRTCData);
        }

        /* Wait for 50ms */
        Delay10KTCYx(40);

    };

    
    /* UART pin definitions and directions */
 //   setup_iBus_UART_hardware (); 
 //   setup_Vm_UART_hardware (&CDPlayer1);
    
    // Open & setup LCD
    // setup_LCD_hardware();
    
    /* setup mode on Vmusic */
    //init_Vmusic (); 
    
    /* Wait for VM to become ready... this needs to be replaced by read from UART at some point... */
 //   Delay10KTCYxx (255);
 //   Delay10KTCYxx (255);
 //   Delay10KTCYxx (255);
    // Delay10KTCYxx (255);
   
 //   workingFrame.source = 0xAA;

    /* Check for hardware line from VM2 to see if it is ready */
    //while (VM_READY != VM_READY) ;
    
    /* Send a frame to the iBus to say that the 'CD-Changer' is ready */
    // memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_UNPOLLED_ANNOUNCE]), 0x24);

    /* Fill the CRC byte */
    // calcCRC(&workingFrame);

    // display_Text(0);
    
    /* Keep sending it until you don't get a collision */ 
 //   while (write_iBus(&workingFrame) != CODE_VALID)
 //       {
 //       wait_iBus_clear_for_15ms();
 //   }
    
 //   while (1)
 //       {
 //       // Wait for a byte to come in from the iBus
 //       read_iBus(&workingFrame);
 //       
 //       if (workingFrame.status == CODE_VALID) // 0x00 is frame valid
 //           {
 //           // Try to match the received frame to one that we recognize
 //           cMatchedFrameID = matchFrame(&workingFrame);
 //           
 //           if (cMatchedFrameID > 0)
 //               {
 //               switch (cMatchedFrameID)
 //                   {
 //                   case 0 : // no valid match
 //                       break;
 //                   case iB_RAD_POLL_CHG : // we rx'd an 'are you there?'; send a 'yes I am'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Send a frame to the iBus to say that the 'CD-Changer' is ready */
 //                       memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_POLLED_ANNOUNCE]), 0x24);
 //                       /* Fill the CRC byte */
 //                       calcCRC(&workingFrame);
 //                       /* Wait til iBus has been high for 15ms then keep sending it until you don't get a collision */ 
 //                       do { wait_iBus_clear_for_15ms(); } while (write_iBus(&workingFrame) != CODE_VALID);
 //                       break;
 //                    case iB_RAD_CURRENT_CHG_STATE : // we rx'd a 'current status?'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Get the right response frame... */
 //                       if (CDPlayer1.status > 0x01) { memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_QUERY_REPLY_NOT_PLAYING]), 0x24); }
 //                       else { memcpypgm2ram (&workingFrame, &(TxFrames[iB_CHG_QUERY_REPLY_PLAYING]), 0x24); }
 //                       /* ... then fill it with the correct track and disk ID ... */
 //                       workingFrame.data[6] = CDPlayer1.current_disk;
 //                       workingFrame.data[7] = CDPlayer1.current_track;
 //                       /* Fill the CRC byte */
 //                       calcCRC(&workingFrame);
 //                       /* Send the response */
 //                       do { wait_iBus_clear_for_15ms(); } while (write_iBus(&workingFrame) != CODE_VALID);
 //                       break;
 //                    case iB_RAD_CHR_STOP : // we rx'd a 'stop!'
 //                        /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       break;
 //                   case iB_RAD_CHR_START : // we rx'd a 'start!'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Pause the VMusic2 */
 //                       VM_UART_write (VM2_PAUSE);
 //                       break;
 //                   case iB_RAD_CHR_GOTO_CD : // we rx'd a 'goto DD'
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       break;
 //                   case iB_RAD_CHR_NEXT_TRACK : // jump to next track
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Next track on the VMusic2 */
 //                       VM_UART_write (VM2_NEXT);
 //                       break;
 //                   case iB_RAD_CHR_PREV_TRACK : // jump back a track
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);
 //                       /* Previous track on the VMusic2 */
 //                       VM_UART_write (VM2_PREV);
 //                       break;
 //                   case iB_MFD_VOL_DN :
 //                       /* Display the received frame on the LCD */
 //                       display_Text(cMatchedFrameID);          
 //                       break;
 //                   default :
 //                       break;
 //               }
 //           }   
 //       }
 //   }    
}
