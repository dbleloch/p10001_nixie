/***************************************************************
*  Name    : LCD 1.c                                           *
*  Author  : Owen O' Hehir                                     *
*  Notice  : Copyright (c) 2005                                *
*          : All Rights Reserved                               *
*  Date    : 17/3/2006                                         *
*  Version : 1.0                                               *
*  Notes   : To startup and run an LCD on a PICDEM 2 Plus Board*
*          : Extended mode opertaion not selected 			   *
*		   :   ===> traditional library          			   *
*          : Small code model selected (<64k program memory)   *
*          : Large data model selected                         *
*          : 18F452 running on 4Mhz canned oscillator          *
****************************************************************
*
* Note XLCD.h & openlcd.c have been altered!!
* Ensure that openxlcd.c, wcmdxlcd.c & writdata.c, setddram.c,
* busyxlcd.c are included as source files in project
*/


/**************************************************************
*  LCD Operations : using WriteCmdXLCD(....)                  *
*                                                             *
*                                                             *
*  DOFF Turn display off                                      *
*  CURSOR_OFF Enable display with no cursor                   *
*  BLINK_ON Enable display with blinking cursor               *
*  BLINK_OFF Enable display with unblinking cursor            *
*  SHIFT_CUR_LEFT Cursor shifts to the left                   *
*  SHIFT_CUR_RIGHT Cursor shifts to the right                 *
*  SHIFT_DISP_LEFT Display shifts to the left                 *
*  SHIFT_DISP_RIGHT Display shifts to the right               *
*                                                             *
***************************************************************
*/

#include <p18F452.h>
#include <stdlib.h> 
#include <xlcd.h>			/*LCD library*/
#include <delays.h>			/*Delay library*/

#define LED0 PORTBbits.RB0	// Led

// As per XLCD included these delay routines:

void DelayFor18TCY(void)
{
Delay10TCYx(0x2);	//delays 20 cycles
return;
}

void DelayPORXLCD(void)   // minimum 15ms
{
Delay100TCYx(0xA0);	  // 100TCY * 160
return;
}

void DelayXLCD(void)     // minimum 5ms
{
Delay100TCYx(0x36);      // 100TCY * 54
}

void main (void)
{	 	
 	TRISB = 0b00000000;					// Setup PORTB as output for LED's
 	
 	// Open & setup LCD
 	OpenXLCD(FOUR_BIT&LINES_5X7);		// Init the LCD Display
	
	while(BusyXLCD()); 
	putrsXLCD("Nesting");		// Write a string variable 

	Delay10KTCYx(255);

	WriteCmdXLCD( SHIFT_CUR_LEFT );
        WriteCmdXLCD( SHIFT_CUR_LEFT );
        WriteCmdXLCD( SHIFT_CUR_LEFT );

    putrsXLCD("birds");		// Write a string variable
	
	LED0=1;

while(1);

}
