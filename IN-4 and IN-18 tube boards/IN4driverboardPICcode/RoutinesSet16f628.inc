;*********************************************************************
; PIC16F6x8A	MACROS and Routines 
;*********************************************************************

BANK0	MACRO
  ERRORLEVEL 	+302
  	bcf			STATUS,RP0		; select bank 0, 
	bcf			STATUS,RP1   	; 000 page  (000)
	
 ENDM

BANK1	MACRO
    ERRORLEVEL 	-302
	bsf			STATUS,RP0		; select bank 1, bits <7,6>=0 <5>=1	
	bcf			STATUS,RP1  	; 001 page  (001)
	
 ENDM

BANK2	MACRO
    ERRORLEVEL 	-302
	bcf			STATUS,RP0		; select bank 1, bits <7,5>=0 <6>=1	
	bsf			STATUS,RP1   	; 002 page  (010)

 ENDM


org 0x005 ; Subroutines
;-----------------------------------------------------------
; N miliseconds delay 0-255ms
;input: delay value in W
;output; delay W miliseconds
DelayNmS:
		movwf	TMP12
			movlw	0xF9		; Internal loop delays for 1.001ms
			movwf	TMP11
			nop
			decfsz	TMP11,f
			goto	$-2			
		decfsz	TMP12,f
		goto	$-6			
		nop					; total delay is 35.001ms
	return



	
; Binary one byte converts in to BCD 2 bytes - Ones and Tens
; Input: bynary byte to convert in W
; Output: TMP01R, TMP02R bytes contain 2 BCD digits
_bin2bcd: BANK0                       ; void bcd_convert(ACCU) {
		clrf TMP02R		;   Tens=0
		movwf TMP01R		;   Ones = W;
gtenth:                          ;   do {
		movlw 0x0a		;     ACCU = 10;
		subwf TMP01R,0		;     ACCU = LSD - ACCU;
		BTFSS STATUS,C		;     if (ACCU < 0)
		goto $+4		;       return;
		movwf TMP01R		;     LSD = ACCU;
		incfsz TMP02R, F		;     MSD++;
		goto gtenth		;   }while(1);
;at this point each decimal digit stored in one byte, 
;means that bits 4-7 are not in use.
;below codes are for merging 2 decimal digits into one byte
;		swapf   TMP02,w
;		addwf	TMP01,f	
;	;	movwf	BCDDigit
		retlw	0		; } return, all done , takes 8-80us
;----------------------------------------------------------

