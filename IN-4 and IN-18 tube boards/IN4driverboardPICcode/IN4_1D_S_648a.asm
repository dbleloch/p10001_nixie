;******************************************************************************
; IN-18 1 DIGIT COUNT DOWN COUNTER SAMPLE CODE BY KOSBO.COM
; FULLY WORKING ASSEMBLER CODE FOR MICRICHIP PIC16F648A MICRICONTROLLER
; TO USE with KOSBO.COM IN-18 HV DRIVER BOARD
; ZERO-ERROR ONE SECOND TIMER BY ROMAN BLACK
; INTERRUPT VERSION

;******************************************************************************

; MPLAB stuff here

	LIST b=5, n=97, t=ON, st=OFF 
	errorlevel -302
	; absolute listing tabs=5, lines=97, trim long lines=ON, symbol table=OFF

;==============================================================================

        LIST    p=16F648a
        INCLUDE P16F648a.INC

		

;                          MCLR off  PWRT off
;                         LVP off |  |WDT off
;                               | |  ||
        __CONFIG 1F09H ;01.1111.0000.1001 ; Vcc=4,5v 	
;                       |     |  | -   -- XT crystal =4.000MHz 
;                      CP on  | BODEN off
;                         DP off


;DEFINE RAM LOCATIONS:

; Variables here

;  											; (pin15,35) Vdd +5v Positive suply
;  											; (pin5,25) Vss Gnd
; PORTA 0-2 to display Digits 1 to 4 
;#define 	NC					PORTA,5		;  (pin4) Input: 
;#define 	NC					PORTA,4		;  (pin3) Output:
;#define 	NC					PORTA,3		;  (pin2) Output:  
#define 	C1					PORTA,2		;  (pin1) Output: 0-on, Display matrix Column 1
#define 	C3					PORTA,1		;  (pin18) Output: 0-on, Display matrix Column 3
#define 	C2					PORTA,0		;  (pin17) Output: 0-on, Display matrix Column 2



; PORTA 6,7 Crystal connection 4MHz 

; PORTB 4-7 to display digit 
#define 	R2					PORTB,7		;  (pin13) Output: 1-on, Display matrix Raw 2
#define 	C5					PORTB,6		;  (pin12) Output: 0-on, Display matrix Column 5
#define 	C4					PORTB,5		;  (pin11) Output: 0-on, Display matrix Column 4
#define 	R1					PORTB,4		;  (pin10) Output: 1-on, Display matrix Raw 1
#define 	Alarm12FC			PORTB,3		;  (pin9) Output: Alarm 1 or 2, 1-on, 0-off; Frequency Check Point
;#define 	TX Rs232			PORTB,2		;  (pin8) Output: Rs232 Output to PC /not configured/
;#define 	RX Rs232			PORTB,1		;  (pin7) Input: Rs232 Input from PC /not configured/
;#define 	NC					PORTB,0		;  (pin6) Output: 




CBLOCK 0x20				; start of ram bank 0

			bres_hi			; 24 hi byte of our 24bit variable
			bres_mid		; 25 mid byte
			bres_lo			; 26 lo byte
			w_temp
			status_temp
							
		;	FLAG00			;1A Flags register 00
			PointCount2ms5	; in use to get every 2.56ms point to do Display Scan	
			TMP01R			;1E
			TMP02R			;1F
			TMP11			;1E
			TMP12			;1F

			CountDown
			DelayCounter
			DPlace
			
ENDC		
	

	org 0x000 			; Program memory page 0 
	goto setup			; 

	org 0x004				; Interrupt vector, int handler code comes next.
	goto	int_handler	
;==============================================================================

;Subroutine block is located here
#INCLUDE 	RoutinesSet16f628.inc


;******************************************************************************
;  INTERRUPT HANDLER     (runs this code each timer0 interrupt) 
;******************************************************************************
;
;------------------
int_handler				; gets here every 2.048mS
;------------------
	BANK0

	;-------------------------------------------------
	; first we preserve w and status register
	movwf 	w_temp      		; save off current W register contents
	movf	STATUS,w          	; move status register into W register
	movwf 	status_temp       	; save off contents of STATUS register

;check if interrupt occured due to TMR0 overflow
	btfss	INTCON,T0IF			; check what interrupt event occured
	goto	int_exit			; another interrupt occured
			; It is TMR0 overflow interruption HAPPENS every 2048uS=2.048mS

;INTERRUPT SECTION *******start*******************************************************************
;display scan  update rounine
			 		BANK0
					decfsz	PointCount2ms5,f
					goto	SkipedDisplayScan		
					movlw	.5				; passed 10.24ms, restore counter
					movwf	PointCount2ms5
							; 10.24ms point, happens every 5x2048us 

; Do Display scan update 
DispScan:			decf	CountDown,w		; Load digit to display
					movwf	DPlace
;TUBE BOARD LAYOUT SPECIFIC CODE /start
;R1=RB4=1, R2=RB7=0
					movlw	0x0A		; 0A code to switch digit off
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bcf		R2
					bsf		C1
					bsf		C2
					bsf		C3
					bsf		C4
					bsf		C5

	movlw	.2		;ok
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bsf		R1
					bcf		R2
					bcf		C1
					bsf		C2
					bsf		C3
					bsf		C4
					bsf		C5

					movlw	.8	;ok
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bsf		R1
					bcf		R2
					bsf		C1
					bsf		C2
					bsf		C3
					bsf		C4
					bcf		C5

					movlw	.0		;ok
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bsf		R1
					bcf		R2
					bsf		C1
					bcf		C2
					bsf		C3
					bsf		C4
					bsf		C5

					movlw	.7		
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bsf		R1
					bcf		R2
					bsf		C1
					bsf		C2
					bcf		C3
					bsf		C4
					bsf		C5

					movlw	.9		
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bsf		R1
					bcf		R2
					bsf		C1
					bsf		C2
					bsf		C3
					bcf		C4
					bsf		C5


;--------------------------------
;R1=RB4=0, R2=RB7=1
					movlw	.1	;ok
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bsf		R2
					bsf		C1
					bsf		C2
					bcf		C3
					bsf		C4
					bsf		C5
		
					movlw	.3	;ok
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bsf		R2
					bcf		C1
					bsf		C2
					bsf		C3
					bsf		C4
					bsf		C5

					movlw	.5	;0k
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bsf		R2
					bsf		C1
					bcf		C2
					bsf		C3
					bsf		C4
					bsf		C5

					movlw	.6	;ok	
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bsf		R2
					bsf		C1
					bsf		C2
					bsf		C3
					bsf		C4
					bcf		C5

					movlw	.4		
					subwf	DPlace,w
					btfss	STATUS,Z
					goto	$+8
					bcf		R1
					bsf		R2
					bsf		C1
					bsf		C2
					bsf		C3
					bcf		C4
					bsf		C5
;TUBE BOARD LAYOUT SPECIFIC CODE /end

DispScanEx:			nop

SkipedDisplayScan 	nop; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




; TMR0 OVERFLOW INTERRUPT OCCURED!!! ******start***************************************************
; TMROintr
; Note! we get here every 2048 instructions, we can now do our special one second timing system	
; This consists of three main steps;
; * subtract 256 counts from our 24bit variable
; * test if we reached the setpoint
; * if so, add 1,000,000 counts to 24bit variable and generate event.
;-------------------------------------------------
; * optimised 24 bit subtract here. This is done with the minimum instructions.
; We subtract 256 from the 24bit variable by just decrementing the mid byte.

		tstf 	bres_mid			; first test for mid==0
		skpnz				; nz = no underflow needed
		decf 	bres_hi,f			; z, so is underflow, so dec the msb
	
		decfsz 	bres_mid,f		; dec the mid byte (subtract 256)
	
; now the full 24bit optimised subtract is done! 
;this is about 4 times faster than a "proper" 24bit subtract.
	
		goto 	int_exit		; nz, so definitely not one second yet.
								; in most cases the entire int takes
								; only 16 instructions.
		;------------------------
						; * test if we have reached one second.
						; only gets here when mid==0, it MAY be one second.
						; only gets to here 1 in every 256 times.
						; (this is our best optimised test)
						; it gets here when bres_mid ==0.

		movf 	bres_hi,f			; test hi for zero too
		btfss	STATUS,Z		; z = both hi and mid are zero, is one second!
		goto 	int_exit		; nz, so not one second yet.
		
	

		
	;******************************************************
	; Only gets to here if we have reached one second.
	;------------------------------------------------------
	; The other thing we need to do is add 1,000,000 counts
	; to our 24bit variable and start all over again.
	;******************************************************
	; Add the 1,000,000 counts first. One second = 1,000,000 = 0F 42 40 (in hex)
	; As we know hi==0 and mid==0 this makes it very fast.This is an optimised 24bit add, 
	; because we can just load the top two bytes and only need to do
	; a real add on the bottom byte. This is much quicker than a "proper" 24bit add.

			movlw 0x01			; get msb value 
			movwf bres_hi			; load in msb
		
			movlw 0xE8			; get mid value
			movwf bres_mid			; load in mid
		
			movlw 0x48			; lsb value to add
			addwf bres_lo,f		; add it to the remainder already in lsb
			skpnc				; nc = no overflow, so mid is still ok
		
			incf bres_mid,f		; c, so lsb overflowed, so inc mid
								; this is optimised and relies on mid being known
								; and that mid won't overflow from one inc.
		
								; that's it! Our optimised 24bit add is done,
								; this is roughly twice as quick as a "proper"
								; 24bit add.
			;*****************************
			;****** 1 sec passed *********
			;*****************************
								; now we do the "event" that we do every one second.
;SkipedDisplayScan	
; here we do time counting every second


		decfsz	DelayCounter,F
		goto	int_exit
		movlw	.1				; set number of seconds for time interval
		movwf	DelayCounter	
		
		decfsz	CountDown,F
		goto	int_exit
		movlw	.10
		movwf	CountDown
		goto	int_exit

	
;-------------------------------------------------------------------------------------------------
; now our one second event is all done, we can exit the interrupt handler.
		
;	goto	int_exit

int_exit:			BCF INTCON,T0IF		; reset the tmr0 interrupt flag
										
					movf status_temp,w     	; retrieve copy of STATUS register
					movwf STATUS            	; restore pre-isr STATUS register contents
					swapf w_temp,f
					swapf w_temp,w          	; restore pre-isr W register contents
					retfie				; return from interrupt
; TMR0 OVERFLOW INTERRUPT OCCURED ******end*********************************************************



;INTERRUPT SECTION *******end*******************************************************************







;******************************************************************************
;  SETUP     (runs only once at startup)
;******************************************************************************
;
;------------------
setup:					; goto label
;------------------
	;-------------------------------------------------
	; Note! here we set up peripherals and port directions.
	; this will need to be changed for different PICs.
	;-------------------------------------------------
						; OPTION setup
	movlw b'10000010'		;
	  	 ;  x-------		; 7, 0=enable, 1=disable, portb pullups
		 ;  -x------		; 6, 1=/, int edge select bit, set on falling edge!!!
		 ;  --x-----		; 5, timer0 source, 0=internal clock, 1=ext pin.
		 ;  ---x----		; 4, timer0 ext edge, 1=\
		 ;  ----x---		; 3, prescaler assign, 1=wdt, 0=timer0 <-selected
		 ;  -----x--		; 2,1,0, timer0 prescaler rate select
		 ;  ------x-		;   000=2, 001=4, 010=8 <-selected, 011=16, etc.
		 ;  -------x		; 
		    				; Note! We set the prescaler to the wdt, so timer0
							; has NO prescaler and will overflow every 256 
							; instructions and make an interrupt.
							;
			BANK1
			movwf OPTION_REG		; load data into OPTION_REG			

			BANK0
			clrf    T1CON        ; ���� TMR1, �������� 1:1
	        clrf    T2CON        ; ���� TMR2
	        movlw   0x07          ; 0000.0111
	        movwf   CMCON        ; Comparators Off
	        clrf    CCP1CON      ; CCP is off
			clrf 	RCSTA		; disable Async Reception
	        
			movlw 	b'00000000'  ; load init data to portA 
			movwf	PORTA
			movlw 	b'00000000'  ; load init data to portB
			movwf	PORTB

			
			BANK1
			clrf	PIE1
			movlw 	b'00100000' 
	        movwf   TRISA        ; PortA - RA5 inputs, others - output
	 		movlw 	b'00000000'  ; All outputs 
	        movwf 	TRISB 
	        clrf	TXSTA       ; disable Async Transmission, 
	        			
			BANK0
	;		movlw	0xFF
	;		movwf	OnOff3sCount	; set 2.5sec On/Off Counter value
		;   movwf	PORTA
	;		bcf		TwosecToggleF

				



				movlw	.9
				movwf	CountDown

				movlw	.1				; set number of seconds for time interval
				movwf	DelayCounter
			

				movlw	.10				; Set counter to get 2.56ms point to do Display Scan
				movwf	PointCount2ms5

			;		movlw	b'11111110'
			;		movwf	DScanShift
					movlw	0x25		; Set Offset for indirect register access for displaying bytes	
					movwf	FSR
			;		movlw	.04
			;		movwf	DScanCount


;-------------------------------------------------
	; Note! Now the hardware is set up we need to load the variables
;;-------------------------------------------------
;
				BANK0

;-------------------------------------------------
	; Note! Now the hardware is set up we need to load the
	; first count for one second into our 24bit bres variable.
	; Note! This example uses 4 MHz clock, which is 1,000,000 counts per second.
	; We require a 1 second period, so we must load 1,000,000 counts each time.
	; 1,000,000 = 0F 42 40 (in hex)
	;
	; We also need to add 256 counts for the first time, so we just add 1 to the mid byte.
	; Check mid overflow if needed.
;-------------------------------------------------
; here we load the 24bit variable.
				movlw 0x01			; get msb value 
				movwf bres_hi			; load in msb
				movlw 0xE8 +1			; get mid value
				movwf bres_mid			; load in mid
				movlw 0x48			; lsb value to add
				movwf bres_lo			; put in mid



; activate interruption
				movlw b'10100000'		; GIE=on TOIE=on, INTE=on (timer0 overflow int, RB0/INT external int)
				movwf INTCON			; set up.


;-----------------------------------------------------------
		; now setup is complete, we can start execution.

				goto main				; start main program

;;-------------------------------------------------------------------------------------------------------

;--------------------------------------------------------------------------------------------------------

;--------------------------------------------------------------------------------------------------------

;--------------------------------------------------------------------------------------------------------

;********************************************************************************************************
;  MAIN     (main program loop)
;********************************************************************************************************
;					
;------------------
main				BANK0		; goto label
;------------------
					

	
			
				movlw	.10
				call	DelayNmS		; delay 9ms to make 10ms main loop
				

				goto	main
				
;==============================================================================
	end					; no code after this point.
;==============================================================================
; 2010 KOSBO.COM





