import math

def getNPointsOnCircle( radius = 10, n = 10 ):
    
    alpha = math.pi * 2 / n
    xpoints = []
    ypoints = []

    
    for i in range(n):
        theta = alpha * i;
        xpoints.append ( math.cos( theta ) * radius )
        ypoints.append ( math.sin( theta ) * radius )
                
    return xpoints, ypoints
    
print getNPointsOnCircle()